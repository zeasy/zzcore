//
//  main.m
//  ZZCore
//
//  Created by EASY on 15/8/20.
//  Copyright (c) 2015年 Z.EASY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
