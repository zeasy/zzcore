//
//  AppDelegate.h
//  ZZCore
//
//  Created by EASY on 15/8/20.
//  Copyright (c) 2015年 Z.EASY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

